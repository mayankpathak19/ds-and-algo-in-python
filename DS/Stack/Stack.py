class Stack:
    def __init__(self):
        self.__stack = list()
    def get_size(self):
        return len(self.__stack)
    def get_top(self):
        pass
    def is_empty(self):
        return len(self.__stack) == 0
    def push(self, data):
        self.__stack.append(data)
    def pop(self):
        return self.__stack.pop()
    def peek(self):
        return self.__stack[self.get_size() - 1]
    def print_stack(self):
        for i in self.__stack:
            print(i)
