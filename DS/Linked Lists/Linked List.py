class Node:
    """
        Class template for a node of a Linear Linked List
    """
    def __init__(self, data=None, next_node=None):
        self.__data = data
        self.__next_node = next_node
    def get_data(self):
        """
            Gets the data item of a node
            @return: node -> data
        """
        return self.__data
    def get_next(self):
        """
            Gets the next node of the current node
            @return: node -> next
        """
        return self.__next_node
    def set_data(self, data):
        """
            Sets the data item of a node
        """
        self.__data = data
    def set_next(self, next_node):
        """
            Sets the next node of the current node
        """
        self.__next_node = next_node

class LinearLinkedList:
    """
        Class template for Linear Linked List
    """
    def __init__(self):
        self.__head = None
    def is_empty(self):
        """
            Checks if the list is empty
            @return: boolean value telling if the Linear Linked List is empty or not
        """
        result = False
        if self.head is None:
            result = True
        else:
            return result
    def append(self, data):
        """
            Appends the data at the end of Linear Linked List
        """
        head_copy = self.__head
        temp_node = Node(data)
        if self.__head is None:
            self.__head = temp_node
        else:
            while head_copy.get_next() is not None:
                head_copy = head_copy.get_next()
            head_copy.set_next(temp_node)
    def insert(self, data, position):
        """
            Inserts the data at particular position in the Linear Linked List
            Indexing starts with 0
        """
        head_copy = self.__head
        temp_node = Node(data)
        count = 0
        while count < position:
            count += 1
            head_copy = head_copy.get_next()
        temp_node.set_next(head_copy.get_next())
        head_copy.set_next(temp_node)
    def print_list(self):
        """
            Prints the Linear Linked List
        """
        head_copy = self.__head
        while head_copy is not None:
            print(head_copy.get_data())
            head_copy = head_copy.get_next()
    def contains(self, value):
        """
            Checks if the Linear Linked List contains an element or not
            @return: boolean value telling if the Linear Linked List contains the value or not
        """
        head_copy = self.__head
        found = False
        while head_copy is not None:
            if head_copy.get_data() == value:
                found = True
                break
            else:
                head_copy = head_copy.get_next()
        return found
    def remove(self, value):
        """
            Removes first appearance of value in the Linear Linked List
        """
        head_copy = self.__head
        previous = None
        found = False
        while not found:
            if head_copy.get_data() == value:
                found = True
            else:
                previous = head_copy
                head_copy = head_copy.get_next()
        if previous == None:
            self.__head = head_copy.get_next()
        else:
            previous.set_next(head_copy.get_next())
    def reverse(self):
        """
            Reverses the Linear Linked List
        """
        previous = None
        head_copy = self.__head
        while head_copy is not None:
            next_node = head_copy.get_next()
            head_copy.set_next(previous)
            previous = head_copy
            head_copy = next_node
        self.__head = previous
